package com.oaker.hours.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.oaker.hours.doman.dto.MhArchiveQueryDTO;
import com.oaker.hours.doman.entity.MhArchive;
import com.oaker.hours.doman.vo.MhArchiveDetailVO;
import com.oaker.hours.doman.vo.MhArchiveVO;

import java.time.LocalDate;
import java.util.List;

public interface MhArchiveService {
    List<MhArchiveVO> queryList(MhArchiveQueryDTO mhArchiveQueryDTO);

    long create(Long deptId, String archiveDate);

    MhArchiveDetailVO query(Long archiveId);

    void deleteById(Long archiveId);

    Integer selectCount(EntityWrapper<MhArchive> queryWrapper);
}
